<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Videolibrary extends Model
{
    use HasFactory;
    protected $fillable=[
        'videoname','exactlink','link',
    ];
}
