<?php

namespace App\Http\Controllers\Group;

use App\Http\Controllers\Controller;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public  function create(){
        return view('group.index');
    }
    public  function create_group(Request $request){
        if(Auth::user())
        {
            $user_id=Auth::user()->id;

         $group=new Group();
         $group->name=$request->input('name');
         $group->description=$request->input('description');
         $group->user_id=$user_id;
         $group->save();
         return  redirect('/video_save/posted_videos')->with('message','Group created successfully');
        }
        else{
          return redirect('')->with('message','Login first to create group');
        }
    }
    }

