<?php

namespace App\Http\Controllers\Documents;

use App\Http\Controllers\Controller;
use App\Models\Document;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    public function savedocument(Request $request){
        $data=new Document();
        $file=$request->file;
        $filename=time().'.'.$file->getClientOriginalExtension();
        $request->file->move('document',$filename);
        $data->file=$filename;
        $data->document=$request->doc;
        $success=$data->save();
        if($success){
            echo "saved succeesfully";
        }

    }
    public function document(){
        $data=Document::all();
        return view('frontend.document',compact('data'));
    }
    public  function viewdocument($id){
        $data=Document::find($id);
        return view('frontend.view',compact('data'));
    }
    public  function  download(Request $request, $file){
        return response()->download(public_path('document/'.$file));
    }

    public function upload(){
        return view('frontend.upload');
    }
    public function posted_document(){
       $data=Document::all();
        return view('frontend.groups_document',compact('data'));
    }
}
