<?php

namespace App\Http\Controllers\Own;

use App\Http\Controllers\Controller;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OwnController extends Controller
{
    public function myvideos(){
        return view('myvideos');
    }
    public function uploadmyvideos(){
        if(Auth::user()){
            $user_id=Auth::user()->id;
            $groups=Group::where('user_id',$user_id)->get();


            return view('group.uploadtomyvideos',compact('groups'));
        }
        else{

        }

    }
}
