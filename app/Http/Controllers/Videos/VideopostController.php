<?php

namespace App\Http\Controllers\Videos;

use App\Models\Group;
use App\Models\Videolibrary;
use Illuminate\Support\Facades\Auth;

class VideopostController
{
    public function posted_videos(){
        if(Auth::user())
        {
            $groups=Group::all();
            return view('frontend.groups_videos',compact('groups'));
        }
        else{
            return redirect('/')->with('message','Login to access this site');
        }
    }
}
