@extends('layouts.app')
@section('title','yourlibrary')
@section('links')
    <li class="nav-item">
        <a class="nav-link" href="video_blogs">Videos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('saved_document')}}">Documents</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="">Contact us</a>
    </li>
@endsection
@section('content')

<div class="container">
    @if(session('message'))
        <h6 class="alert alert-warning mb-3">{{session('message')}}</h6>
    @endif
<div class="row">
    <div class="col home_view">
        <h2 class="m-2">Your home of softcopy resources</h2>
        <p class="mt-1">This is where your contents are kept anything you need are all found here.</p>

        <p>Thanks for joining ug together we can do extraordinary.</p>
        <span class="mt-4">
        <button class="btn btn-outline-primary text-white">Register Today</button>
        <button class="btn btn-outline-primary text-white ms-4">Login</button>
        </span>
    </div>
</div>
</div>

@endsection
