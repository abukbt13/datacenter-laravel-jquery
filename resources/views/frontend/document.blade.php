@extends('layouts.app')
@section('title','videoblogs')
@section('links')
    <li class="nav-item">
        <a class="nav-link" href="{{url('saved_document/posted_document')}}">Groups Documents</a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{url('video_blogs')}}">Videos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('upload')}}">Post video/link</a>
    </li>
@endsection
@section('content')
    <div class="container">


        <table>
            <tr>
                <th>id</th>
                <th>name</th>
                <th>Document</th>
                <th>Download</th>
                <th>View</th>
            </tr>
            @foreach($data as $data)
                <tr>
                    <td>{{$data->id}}</td>
                    <td>{{$data->document}}</td>
                    <td>{{$data->file}}</td>
                    <td><a href="{{url('download',$data->file)}}">Download</a></td>
                    <td><a href="{{url('view',$data->id)}}">View</a></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

