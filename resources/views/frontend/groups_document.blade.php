@extends('layouts.app')
@section('title','posted document')
@section('links')
    <li class="nav-item">
        <a class="nav-link" href="{{url('create_group')}}">Create Group</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('video_blogs')}}">Videos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('upload')}}">Post video/link</a>
    </li>
@endsection
@section('content')

    <div class="container">
        <style>

            .videochoice{
                border: solid 2px grey;
                /*height: 100vh;*/
                background-color: #F4F4FB;

            }
            .videosbg{
                /*background-color: #FBBB2B;*/
                min-height: 85vh;

                overflow: hidden;
                overflow-y: scroll;
            }

            .videos{
                margin-top: 3px ;
                width: 100%;
            }

            .span{
                /*background-color: rgba(216, 240, 238, 0.589);*/
                width: 100%;
                /*color: blue;*/
                position: relative;
            }

            .play{
                position: relative;
                bottom:0.2rem;
                background-color: #7F7C9B;
                padding: 4px;
                width: 100%;
                margin-left: 1rem;
                text-align: center;
                text-transform: uppercase;
                color: white;
                cursor: pointer;
                transition: all .2s ease-in-out;

            }




            .play:hover{
                background-color: #FBBB2B;

                /*font-size: 24px;*/

            }
            .videos{
                padding-top: 3px;
                display: grid;
                grid-template-columns:  1fr 1fr 1fr ;
                gap: 0.5rem;
            }
            .videoplayhere{
                width: 100%;
                height: 26rem;
                border:solid;
                display: none;
            }


        </style>
        <div class="row">
            <div class="col-md-6 videochoice">

                <div class="videosbg">
                    <div class="row bg-light">
                        <div class="col">
                            <button class="btn btn-outline-primary float-left mt-2">MyVideos</button>
                        </div>
                        <div class="col">
                            <button class="btn btn-outline-primary float-end mt-2">Mydocuments</button>
                        </div>
                    </div>
                    <div class="videos">

                        @foreach($data as $data)
                            <tr>
                                <td>{{$data->id}}</td>
                                <td>{{$data->document}}</td>
                                <td>{{$data->file}}</td>
                                <td><a href="{{url('download',$data->file)}}">Download</a></td>
                                <td><a href="{{url('view',$data->id)}}">View</a></td>
                            </tr>
                        @endforeach

                    </div>
                </div>

            </div>

            <div class="col-md-6">

                <div class="explore">
                    <H>Interact with friends here</H>
                    <p>By joining a group you are  be able to learn and enjoy the contents that are found there.</p>
                    <p>We also allow you to create a group where you are able to store your own resource </p>
                    <p>Thanks for choosing our library as a place to learnt</p>
                </div>
            </div>
        </div>



    </div>
@endsection


