@extends('layouts.app')
@section('title','group')
@section('links')
    <li class="nav-item">
        <a class="nav-link" href="{{url('create_group')}}">Create Group</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('saved_document')}}">Documents</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('upload')}}">Post video/link</a>
    </li>
@endsection
@section('content')

    <div class="container">
        <style>

            .videochoice{
                border: solid 2px grey;
                /*height: 100vh;*/
                background-color: #F4F4FB;

            }
            .videosbg{
                /*background-color: #FBBB2B;*/
                min-height: 85vh;

                overflow: hidden;
                overflow-y: scroll;
            }

            .videos{
                margin-top: 3px ;
                width: 100%;
            }

            .span{
                /*background-color: rgba(216, 240, 238, 0.589);*/
                width: 100%;
                /*color: blue;*/
                position: relative;
            }

            .play{
                position: relative;
                bottom:0.2rem;
                background-color: #7F7C9B;
                padding: 4px;
                width: 100%;
                margin-left: 1rem;
                text-align: center;
                text-transform: uppercase;
                color: white;
                cursor: pointer;
                transition: all .2s ease-in-out;

            }




            .play:hover{
                background-color: #FBBB2B;

                /*font-size: 24px;*/

            }
            .videos{
                padding-top: 3px;
                display: grid;
                grid-template-columns:  1fr 1fr 1fr ;
                gap: 0.5rem;
            }
            .videoplayhere{
                width: 100%;
                height: 26rem;
                border:solid;
                display: none;
            }


        </style>
        <div class="row">
            <div class="col-md-6 videochoice">
                @if(session('message'))
                    <h6 class="alert alert-warning mb-3">{{session('message')}}</h6>
                @endif
                <div class="videosbg">
                    <div class="row bg-light">
                        <div class="col">
                            <a href="{{url('myvideos')}}" class="btn btn-outline-primary">My videos</a>
                        </div>
                        <div class="col">
                            <a href="{{url('create_group')}}" class="btn btn-outline-primary">Create_group</a>
                        </div>
                        <div class="col">
                            <a href="" class="btn btn-outline-primary">Documents</a>                        </div>
                    </div>
                    <div>
                        @foreach($groups as $group)
                        <div style="height: 4rem;width: 100%; background-color: brown; margin-bottom: 7px;">
                            <p>{{$group->name}}</p>
                            <div class="videochoices">
                                @php
                                    $likes = \App\Models\Videolibrary::where('groupname', $group->name)->count();
                                @endphp
                                <p>{{$likes}}</p>

                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>

            <div class="col-md-6">
                <div  class="videoplayhere">
                    <iframe width="100%" id="videoplayer" height="100%" src=""
                            rameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>
                <div class="explore">
                    <H>Interact with friends here</H>
                    <p>By joining a group you are  be able to learn and enjoy the contents that are found there.</p>
                    <p>We also allow you to create a group where you are able to store your own resource </p>
                    <p>Thanks for choosing our library as a place to learnt</p>
                </div>
            </div>
        </div>



    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.play').click(function (e) {
                e.preventDefault();
                const link=$(this).closest('.span').find('#link').val();
                $('.videoplayhere').show()
                $('#videoplayer').attr('src', "https://www.youtube.com/embed/"+ link +"?>?modestbranding=1")
            });

        });
    </script>
@endsection

