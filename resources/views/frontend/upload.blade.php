@extends('layouts.app')
@section('title','upload')
@section('links')
    <li class="nav-item">
        <a class="nav-link" href="{{url('video_blogs')}}">Videos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('saved_document')}}">Documents</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('group')}}">Groups</a>
    </li>
@endsection
@section('content')
    <div class="container">
            <div style="width: 100%; height: 100vh;background-color: #adb5bd;display: flex;align-items: center;" class="row upload">

                <div class="col-sm-6 d-flex justify-content-center">
                    <form action="{{url('savedocument')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <h5>Upload document here</h5>
                        <label>Document name </label><br>
                        <input type="text" name="doc" placeholder="Ente the video name"><br>
                        <label for="">Upload Document</label><br>
                        <input type="file" name="file" placeholder="Upload document here"><br>
                        <button type="submit" class="btn btn-outline-primary">Upload</button>
                    </form>
                </div>
                <div class="col-sm-6 d-flex justify-content-center">
                    <form action="{{url('video_save')}}" method="post">
                        @csrf
                        <h5>Save Youtube Video here</h5>
                        <input type="text" name="videoname" placeholder="Ente the video name"><br>
                        <label for="">Enter Video Link</label><br>
                        <input type="text" name="link" placeholder="Enter the video link"><br>
                        <button type="submit" class="btn btn-outline-primary">Save The video</button>
                    </form>
                </div>

            </div>
    </div>
@endsection
