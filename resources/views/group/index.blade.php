@extends('layouts.app')
@section('title','upload')
@section('links')
    <li class="nav-item">
        <a class="nav-link" href="{{url('video_blogs')}}">Videos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('saved_document')}}">Documents</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('group')}}">Groups</a>
    </li>
@endsection
@section('content')
    <div class="container">
        <div style="width: 100%; height: 100vh;background-color: #adb5bd;display: flex;align-items: center;" class="row upload">

            <div class="col-sm-12 d-flex justify-content-center">
                <form action="{{url('create_group')}}" method="post">
                    @csrf
                    <h5>Create Group</h5>
                    <label>Enter Groupname</label><br>
                    <input type="text" name="name" placeholder="Enter Groupname"><br>
                    <label for="">Group Description</label><br>
                    <input type="text" name="description" placeholder="Groups description"><br>
                    <button type="submit" class="btn btn-outline-primary">Create Group</button>
                </form>
            </div>



    </div>
@endsection
