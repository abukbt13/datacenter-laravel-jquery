@extends('layouts.app')
@section('title','upload')
@section('links')
    <li class="nav-item">
        <a class="nav-link" href="{{url('video_blogs')}}">Videos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('saved_document')}}">Documents</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('group')}}">Groups</a>
    </li>
@endsection
@section('content')
    <div class="container">
        <div style="width: 100%; height: 100vh;background-color: #adb5bd;display: flex;align-items: center;" class="row upload">

            <div class="col-sm-12 d-flex justify-content-center">
                <form action="{{url('video_save')}}" method="post">
                    @csrf
                    <h5>Upload to my group</h5>
                    <label for="">Enter Videoname</label><br>
                    <input type="text" name="videoname" placeholder="Enter the video name"><br>
                    <label for="">Enter Description(optional)</label><br>
                    <input type="text" name="description" placeholder="Enter video description"><br>
                    <label for="">Enter Video Link</label><br>
                    <input type="text" name="link" placeholder="Enter the video link"><br>
                    <label for="">Choose Group to Upload</label><br>

                    <select class="form form-control" name="groupname" id="">
                        @foreach($groups as $group)
                            <option value="{{$group->name}}">{{$group->name}}</option>
                        @endforeach
                    </select><br>

                    <button type="submit" class="btn btn-outline-primary">Save The video</button>
                </form>
            </div>



        </div>
@endsection
