@extends('layouts.app')
@section('title','videoblogs')
@section('links')
    <li class="nav-item">
        <a class="nav-link" href="{{url('/video_save/posted_videos')}}">Groups Post</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('saved_document')}}">Documents</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('upload')}}">Post video/link</a>
    </li>

@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 videochoice">

                <div class="videosbg">
                    <div class="row bg-light">
{{--                        <div class="col">--}}
{{--                            <a href="{{url('myvideos')}}" class="btn btn-outline-primary">My videos</a>--}}
{{--                        </div>--}}
                        <div class="col">
                            <a href="{{url('uploadmyvideos')}}" class="btn btn-outline-primary">Upload Video</a>
                        </div>
                        <div class="col">
                            <a href="" class="btn btn-outline-primary">Documents</a>                        </div>
                    </div>
                    <div class="videos">
{{--                        @foreach($videos as $video)--}}
                            <div class="span">
                                <span>Videohere</span><br>
                                <input hidden type="text" value="link" id="link">
                                <span class="play">play video</span>
                            </div>
{{--                        @endforeach--}}
                    </div>
                </div>

            </div>

            <div class="col-md-6">
                <div  class="videoplayhere">
                    <iframe width="100%" id="videoplayer" height="100%" src=""
                            rameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>
                <div class="explore">
                    <H>Interact with friends here</H>
                    <p>By joining a group you are  be able to learn and enjoy the contents that are found there.</p>
                    <p>We also allow you to create a group where you are able to store your own resource </p>
                    <p>Thanks for choosing our library as a place to learnt</p>
                </div>
            </div>
        </div>



    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.play').click(function (e) {
                e.preventDefault();
                const link=$(this).closest('.span').find('#link').val();
                $('.videoplayhere').show()
                $('#videoplayer').attr('src', "https://www.youtube.com/embed/"+ link +"?>?modestbranding=1")
            });

        });
    </script>
@endsection
