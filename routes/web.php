<?php


use App\Http\Controllers\Documents\DocumentController;
use App\Http\Controllers\Group\GroupController;
use App\Http\Controllers\Own\OwnController;
use App\Http\Controllers\Videos\VideopostController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SmsController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\FrontendController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//index home route
Route::get('/',[FrontendController::class, 'index'])->name('frontend');


Route::middleware(['auth','isAdmin'])->group(function() {
Route::get('admin',[AdminController::class, 'index'])->name('admin');

});
Route::post('/forgetpassword',[SmsController::class, 'recover']);
Route::get('/forgetpassword',[FrontendController::class, 'view']);
Route::get('video_blogs',[FrontendController::class, 'video_blogs']);
Route::post('/video_save',[FrontendController::class, 'video_save']);





//Videos
Route::get('/video_save/posted_videos',[VideopostController::class,'posted_videos']);


//Documents
Route::get('saved_document',[DocumentController::class,'document']);
Route::post('/savedocument',[DocumentController::class,'savedocument']);
Route::get('saved_document/posted_document',[DocumentController::class,'posted_document']);
Route::get('/view/{id}',[DocumentController::class,'viewdocument']);
Route::get('/download/{file}',[DocumentController::class,'download']);
Route::get('/upload',[DocumentController::class,'upload']);


//Group
Route::get('/create_group',[GroupController::class,'create']);
Route::post('/create_group',[GroupController::class,'create_group']);


//Own
Route::get('/myvideos',[OwnController::class,'myvideos']);
Route::get('/uploadmyvideos',[OwnController::class,'uploadmyvideos']);
